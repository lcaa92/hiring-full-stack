FROM node:6.9.1

MAINTAINER leandrocaaraujo@gmail.com

ENV DEBIAN_FRONTEND noninteractive
ADD ./ /opt/

RUN cd /opt/ && ls -l

RUN apt-get update && apt-get install -y apache2 zip unzip git curl software-properties-common python-software-properties git-core && apt-get clean
RUN apt-get install apt-transport-https lsb-release ca-certificates && wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg && echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list && apt-get update && apt-get -y install php7.1 php7.1-common php7.1-cli libapache2-mod-php7.1 php7.1-mysql php7.1-curl php-memcached php7.1-dev php7.1-mcrypt php7.1-sqlite3 php7.1-mbstring php7.1-xml && apt-get clean && curl -sS https://getcomposer.org/installer | php && apt-get clean

RUN npm install sql-cli -g
RUN npm install @angular/cli -g
RUN npm install typescript -g
RUN npm install typings -g
RUN npm cache clean && rm -rf ~/.npm
RUN cd /opt/
RUN rm -rf node_modules
RUN ls -l
RUN npm install --production --unsafe-perm
RUN npm cache clean


RUN git clone https://gitlab.com/lcaa92/hiring-full-stack.git && cp /hiring-full-stack/backend/.env.example /hiring-full-stack/backend/.env && cd /hiring-full-stack/backend/ && php ../../composer.phar update && cd ../frontend && ng update

RUN ng -v
EXPOSE 80
EXPOSE 8000
EXPOSE 8001
EXPOSE 4200
WORKDIR /hiring-full-stack/frontend