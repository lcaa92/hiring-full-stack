@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    
                    <form action="{{ route('find.user') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>User:</label>
                            <input type="text" class="form-control" name="user">
                        </div>
                        
                        <input type="submit" class="btn btn-success col-md-12" value="Enviar">
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
