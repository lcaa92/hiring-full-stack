<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function findUser(Request $request){
        //https://github.com/KnpLabs/php-github-api/tree/master/doc
        $client = new \Github\Client();
        $repositories = $client->api('user')->repositories($request->user);
        $user = $client->api('user')->show($request->user);

        echo "<pre>";
        print_r($user);
        echo "</pre>";
        echo "<pre>";
        print_r($repositories);
        echo "</pre>";
    }
}
