<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Exception;

class ApiGithubController extends Controller
{
    public function getDados($account = ''){
        $arrDados = [];

        try {
            $client = new \Github\Client();
            $user = $client->api('user')->show($account);

            $arrDados = [
                ['id' => $user['id'], 'name' => $user['login'] ] 
            ];
        } catch(Exception $e){
            $arrDados = [
                ['id' => '0', 'name' => 'FALHA' ] 
            ];
        }

        return json_encode($arrDados);
    }

    public function getrepositories($account = ''){
        
        $arrRep = [];
        try {
            $client = new \Github\Client();
            $repositories = $client->api('user')->repositories($account);

            foreach ($repositories as $rep){
                array_push($arrRep, [
                    'id' => $rep['id'],
                    'name' => $rep['name'],
                    'full_name' => $rep['full_name'],
                    'html_url' => $rep['html_url']
                ]);
           }
        } catch(Exception $e){
            array_push($arrRep, [
                'id' => 'FALHA',
                'name' => 'FALHA',
                'full_name' => 'FALHA',
                'html_url' => 'FALHA'
            ]);
        }

       return json_encode($arrRep);
    }
}
