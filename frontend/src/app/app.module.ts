import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { GithubService } from './github.service';
import { GithubsearchComponent } from './githubsearch/githubsearch.component';


@NgModule({
  declarations: [
    AppComponent,
    GithubsearchComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    GithubService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
