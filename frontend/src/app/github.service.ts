import { Injectable } from '@angular/core';
import { Github } from './github';
import { Githubrepository } from './githubRespository'
import { DADOS } from './mock-github';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable()
export class GithubService {

  private apiUrl = 'http://apibackend.com.br:8001/api/'; 

  constructor(
    private http: HttpClient,
  ) { }

  getDados(account: string): Observable<Github[]> {
    return this.http.get<Github[]>(this.apiUrl+'data/'+account)
    .pipe(
      catchError(this.handleError('getDados', []))
    );
  }

  getRepositories(account: string): Observable<Githubrepository[]> {
    return this.http.get<Githubrepository[]>(this.apiUrl+'repositories/'+account)
    .pipe(
      catchError(this.handleError('getRepositories', []))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
 
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
