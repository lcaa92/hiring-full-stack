import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Github } from '../github'; 
import { Githubrepository } from '../githubRespository'
import { GithubService } from '../github.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-githubsearch',
  templateUrl: './githubsearch.component.html',
  styleUrls: ['./githubsearch.component.css']
})
export class GithubsearchComponent implements OnInit {

  dados: Github[];
  repositories: Githubrepository[];

  constructor(
    private githubService: GithubService,
    private http: HttpClient,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
  }

  heroes = ['Windstorm', 'Bombasto', 'Magneta', 'Tornado'];
  addHero(newHero: string) {
    if (newHero) {
      this.heroes.push(newHero);
    }
  }

  getGitHubData(account: string){
    console.log(account);
    this.getDados(account);
    this.getRepositories(account);
  }

  getDados(account: string): void {
    this.githubService.getDados(account)
    .subscribe(dados => this.dados = dados);
  }

  getRepositories(account: string): void{
    this.githubService.getRepositories(account)
    .subscribe(repositories => this.repositories = repositories);
  }

}
