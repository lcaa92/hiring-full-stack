export class Githubrepository {
    id: number;
    name: string;
    full_name: string;
    html_url: string;
  }